# maths

A set of Clojure implementation of famous algorithms and fun code samples.

## Usage

More to come from ..

* http://rosettacode.org/mw/index.php?title=Special:Ask&offset=20&limit=20&q=[[Is+task%3A%3Atrue]]&p=mainlabel%3D%2Fformat%3Dbroadtable&sort=_MDAT&order=DESC
* http://en.wikipedia.org/wiki/List_of_algorithms
* https://github.com/acmeism/RosettaCodeData/tree/master/Task

* git@github.com:elben/k-means.git
* http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2865495/

* http://sentiment.christopherpotts.net/lexicons.html
* https://github.com/damionjunk/sentimental

* https://github.com/bmabey/clj-slope-one/blob/master/project.clj
* https://github.com/jmgimeno/okasaki-clojure

## License

Copyright © 2015-2016 Nicolas Modrzyk

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.

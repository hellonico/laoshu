(defproject bomgo "0.1.0-SNAPSHOT"
   :main external.mongo.sample
   :profiles {
    :uberjar {:aot :all}
   }
  :dependencies [[org.clojure/clojure "1.8.0"]

;;                  ; maybe should be a different project
;;                  [postgresql "9.3-1102.jdbc41"]
;;                  [sqlingvo "0.8.4"]
;;                  [org.clojure/java.jdbc "0.4.2"]

;;                  ;TODO to remove ?
;;                  [org.clojure/data.json "0.2.6"]

                 [com.novemberain/monger "3.0.2"]
                 [org.slf4j/slf4j-nop "1.7.14"]
                 [com.climate/claypoole "1.1.1"]

  ])

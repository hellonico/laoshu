(ns external.postgres.sample
  (:require
    [clojure.java.jdbc :as j]
    [external.postgres.json]))

(def spec
  { :subprotocol "postgresql"
    :subname     "//192.168.99.100:5432/postgres"
    :user "postgres"
    :password "mysecretpassword"})

(comment

;;;; CREATE TABLE
(j/db-do-commands spec
  "CREATE TABLE example (data json);")

;;;; INSERT
;;;;
(j/db-do-prepared spec
  "INSERT INTO example (data) VALUES (?);"
  [{:foo "bar"}])

;;;; MANY INSERTS
(pmap
  (fn[_]
    (j/insert! spec :example
  nil
  [{:foo "bar"}]))
  (range 100))

(dotimes [_ 100]
  (j/insert! spec :example
  nil
  [{:foo "bar"}]))


(j/insert! spec :example
  nil
  [{:foo "bar"}]
  [{:foo "bar"}]
  [{:foo "bar"}])

(defn batch-insert [howmany]
  (apply
    (partial j/insert! spec :example nil)
    (map
      #(hash-map :foo (str "bar" %))
      (take howmany (repeat 1)))))

(comment
  (time
    (batch-insert (* 2000 1250)))
; 2000* 1250 => 105062.063411
  )

;;;; QUERY
;;;;
(j/query spec
  ["SELECT count(*) FROM example"]
   :result-set-fn first)

;;;; DELETE
;;;;
(j/delete! spec :example nil)

(j/db-do-commands spec
  (j/drop-table-ddl :example))

  )

;(require '[sqlingvo.core :as sql])
;(require '[sqlingvo.db :as db])
;; (def db (db/postgresql))

;;   (sql/sql (sql/select * :example))

; (j/query spec

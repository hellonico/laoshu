(ns external.mongo.sample
  (:require [monger.core :as mg]
            [monger.collection :as mc])
  (:import com.mongodb.WriteConcern)
  (:require [com.climate.claypoole :as cp])
  (:import org.bson.types.ObjectId)
  (:gen-class))

(declare one-object)

(def sample-object
  "{
    :snapshot  \"irs1\"
    :clearingMember  \"Member A\"
    :level2  \"Client B\"
    :portfolio  \"Porfolio1\"
    :tradeId  \"JSCC~123456\"
    :description  \"Swap 2025-11-24 Pay 1.5329 / Rec JPY LIBOR 6M\"
    :scenarioName  \"Steepen\"
    :pnl  (* 100000 (rand))}
 "
  )

(defn batch-insert [_uri howmany]
  (let [{:keys [conn db]} (mg/connect-via-uri _uri)]
  (mc/insert-batch db "documents"
   (repeatedly howmany one-object))
   (mg/disconnect conn)))

(defn count-col [_uri coll]
  (let [{:keys [conn db]} (mg/connect-via-uri _uri)]
    (mc/count db coll)))

(defn- i[a]
  (Integer/parseInt (str a)))

(defn- install[]
  (println "Creating sample object in object.clj")
      (spit "object.clj" (str
 " (defn one-object [] " sample-object ")")))

(def headers
  "inserts, batch-size, batch, pool, total time, average time, target\n")

(defn mongo-url [host-port-coll]
(str
 "mongodb://"
 host-port-coll
 "?connectTimeoutMS=60000&uri.maxPoolSize=1000"))

(defn run[_uri _report & args]

    (binding [*ns* (the-ns 'external.mongo.sample)]
    (eval (read-string (slurp "object.clj"))))

  (if (not (.exists (clojure.java.io/as-file _report)))
       (spit _report headers))

  (let [
        uri (mongo-url _uri)
         batch (i (nth args 0)) _range (i (nth args 1)) _pool (i (nth args 2)) _start (System/currentTimeMillis)]
    (cp/with-shutdown! [pool
     (cp/threadpool _pool :daemon true :name "my-pool")]
   (println "Connecting to " uri " [ " _pool "threads ] ")

   (time
    (doall

      (cp/pmap pool
        (fn[_] (batch-insert uri batch))
        (range _range))))

  (let [_totalb (* batch _range)
         _timed (- (System/currentTimeMillis) _start) _avg (int (/ _totalb (/ _timed 1000) ))
        _report-line (str _totalb "," batch "," _range "," _pool "," _timed "," _avg "," _uri "\n")
        ]

    (println headers)
    (println _report-line)

   (spit _report
     _report-line :append true)
  ))))

(defn -main[& args]
  (if (not (.exists (clojure.java.io/as-file "object.clj")))
  (install))

  (if (< (count args)  4)
    (println "Usage: -main uri report-file batch-size nb-batch pool-size")
    (apply run args))

  (System/exit 0))



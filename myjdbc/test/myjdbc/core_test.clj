(ns myjdbc.core-test
  (:require [clojure.test :refer :all]
            [myjdbc.core :refer :all]))


(def local
  "mongodb://127.0.0.1/monger-test4&connectTimeoutMS=60000&uri.maxPoolSize=1000")
(def d-local
  "mongodb://192.168.99.100/myjdbc")
(def jupiter
  "mongodb://61.209.205.172/clojure?connectTimeout=120000&socketTimeout=120000")
(def report-file
  "results.csv")


(-main d-local report-file 20000 10 6)l


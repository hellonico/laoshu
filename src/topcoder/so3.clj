(use 'pool.core)


(def pool
  (get-pool
    (fn [] :heavy-object)))

(dotimes [_ 10]
  (borrow pool))

(def object2
  (borrow pool))

(return pool object)



(def pool-2
  (get-keypool (fn [k] (str :heavy k))))

(def object-2
  (borrow pool-2 :object))
; Return key and object
(return pool-2 :object object-2)

(.getNumActive pool)
(.getNumIdle pool)
(.listAllObjects pool)

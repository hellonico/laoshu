(ns topcoder.so2)
; looking for prime numbers up to square root

(defn root-1 [x]
    (inc (long (Math/sqrt x))))
(defn range-1 [x]
  (range 2 (root-1 x)))

(defn filter-1 [x]
  (filter #(zero? (rem x %))
        (range-1 x)))
(defn is-prime [x]
  (nil? (first (filter-1 x))))

(defn -main[& args]
  (time
    (is-prime
      (java.math.BigInteger. (first args)))))

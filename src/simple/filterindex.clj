(defn indices [pred coll]
   (keep-indexed #(when (pred %2) %1) coll))

(first
  (indices pos? [-1 0 99 100 101]))


(keep-indexed
  #(when (= 4 %1) %2)
  [1 2 3 5 6 7])

(defn divides? [N n]
  (zero? (mod N n)))

(defn f-reduce [n f & {:keys [expt] :or {expt 0}}]
  (if (divides? n f) (f-reduce (/ n f) f :expt (inc expt))
                     (if (zero? expt) [n []] [n [f expt]])))

(defn _factors [n f known-fs]
  (let [[m fs] (f-reduce n f)]
    (if (> f (Math/sqrt m))
      (cond (and (empty? fs) (= m 1)) known-fs
            (empty? fs)               (concat known-fs [m 1])
            (= m 1)                   (concat known-fs [f (last fs)])
            true                      (concat known-fs [m (last fs)]))
      #(_factors m (+ 2 f) (concat known-fs fs)))))

(defn factors
  "returns the prime factors of n in form: p_0 expt_0 p_1 expt_1 ... p_m expt_m,
  where p_i denotes ith prime factor, and expt_i denotes exponent of p_i"
  [n]
  (let [[m fs] (f-reduce n 2)]
    (trampoline (_factors m 3 fs))))

; (factors 12424242427)


(defn lazy-primes
  ([] (cons 2 (lazy-seq (lazy-primes 3  [ 2 ]))))
  ([current calculated-primes]
   (loop [ [first-prime & rest-primes] calculated-primes]
     (if (> (* first-prime first-prime) current)
       (cons current (lazy-seq (lazy-primes
                                (inc current)
                                (conj calculated-primes current))))
       (if (= 0 (mod current first-prime))
         (lazy-seq (lazy-primes (inc current) calculated-primes))
         (recur rest-primes))))))

(time (last (take 100011111 (lazy-primes))))
